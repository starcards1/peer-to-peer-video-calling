<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
</head>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css"> 
<link rel="stylesheet" href="https://linqstar.com/css/getHTMLMediaElement.css">
<link rel="stylesheet" href="https://linqstar.com/css/stream.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://linqstar.com/js/fileBufferReader.js"></script>
<script src="https://linqstar.com/js/led.js" async defer></script>
<script src="https://terrawire.com:9001/socket.io/socket.io.js"></script>
<script src="https://linqstar.com/js/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script src="https://rawgit.com/enyo/dropzone/master/dist/dropzone.js"></script>
<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
<style>

.media-box{
	background:transparent!Important;
	border:none!Important;
}
#remote{
	width:300px!Important;
	height:300px!Important;
	border-radius:600px!Important;
	object-fit:contain!;
	position:fixed;
	left:0;
	right:0;
	margin:auto;
	border:10px solid white;
	
	}
#local{
	width:120px!Important;
	height:120px!Important;
	border-radius:600px!Important;
	object-fit:contain!;
	position:fixed;
	left:5%;
	border:10px solid white;
	top:230px;
}
.button-5 {
    border: none;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    background: -moz-linear-gradient(0deg, #e2906e 0%, #e83a99 100%);
    background: -webkit-linear-gradient(0deg, #e2906e 0%, #e83a99 100%);
    background: -ms-linear-gradient(0deg, #e2906e 0%, #e83a99 100%);
    box-shadow: 2.419px 9.703px 12.48px 0.52px rgba(232, 58, 153, 0.5);
    width: auto;
    padding: 10px 45px;
    color: #ffffff;
    margin-top: 20px;
	}
</style>
	<body>
<div style="position:fixed;top:0;left:0;height:100%;width:100%;background:url(https://linqstar.com/pur_bg.png);background-size:cover" id="pur"></div>	
<div id="videos-container"></div>
<div id="demo" style="width:100%;margin:auto;left:0;right:0;text-align:center;position:fixed;height:80px;bottom:-45px;z-index:9999999999999999999;color:white;padding:10px;font-size:44px;font-family:Bungee Outline;display:none"></div>
<img src="https://linqstar.com/assets/images/logo2.png" style="position:fixed;width:200px;margin:auto;left:0;right:0;bottom:75px" align=center>
<a class="button-5" href="javascript:history.go(-1)" id="smode" style="position:fixed;width:125px;margin:auto;right:10px;bottom:15px;padding:15px;text-align:center">End Call</a> <a class="button-5" href="javascript:history.go(0)" id="smode" style="position:fixed;width:125px;margin:auto;left:10px;bottom:15px;padding:15px;text-align:center">Refresh</a>
<script src="https://linqstar.com/js/LushRTC.js"></script>
<script src="https://linqstar.com/rtc/node_modules/webrtc-adapter/out/adapter.js"></script>
<!-- custom layout for HTML5 audio/video elements -->
<script src="https://linqstar.com/rtc/dev/getHTMLMediaElement.js"></script>

<script>

window.getExternalIceServers = true;
var video,localVideo,remoteVideo,localStream,remoteStream,localMediaElement,remoteMediaElement,st,rst
var tokens=10
var countDownDate
var x
// Set the date we're counting down to
	function show_dial() {
		$('#dial').show()
	}
	
	function hide_dial() {
		$('#dial').hide()
	}
	
	function $$(obj) {
		return document.getElementById(obj)
	}
	function qs(name, url) {
		if (!url) {
		  url = window.location.href;
		}
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	var wl  = window.location.href;
	var mob = (window.location.href.indexOf('file://')>=0);

	function setCookie(cname,cvalue)	{
		if (mob===true) {
			window.localStorage.setItem(cname, cvalue);
		} else {
			var d = new Date(); 
			d.setTime(d.getTime()+(1*24*60*60*1000)); 
			var expires = "expires="+d.toGMTString(); 
			document.cookie = cname + "=" + cvalue + "; " + expires; 
		}
	} 

	function getCookie(cname)	{ 
		if (mob===true) {
			var cvalue = window.localStorage.getItem(cname);
			return cvalue
		} else {
			var name = cname + "="; 
			var ca = document.cookie.split(';'); 
			for(var i=0; i<ca.length; i++) { 
			  var c = ca[i].trim(); 
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length); 
			} 
			return "";  
		}
	} 
	function clear_all() {
		if (mob===true) {
			 window.localStorage.clear();
		} else {
			document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
		}	
	}		

	function showFileTransfer() {
		location.href='file?login='+qs('roomid')+'&mobile='+qs('mobile')
	}

	function deduct_balance() {
//		$.ajax({
//			url:'https://lushmatch.com/sdfm/api/x_update_balance.php?mid='+localStorage.getItem('mid')+'&type=video_balance',
//			success:function(data){}
//		})
	}


var codec = 'H264';
var roomid = qs('roomid');
var action = qs('action');
var mobile = qs('mob');

var vlocal,vremote

var connection = new LushRTC();
connection.socketURL = 'https://terrawire.com:9001/';
connection.socketMessageEvent = 'celebrity';
connection.session = {
    audio: true,
    video: true
};
connection.sdpConstraints.mandatory = {
    OfferToReceiveAudio: true,
    OfferToReceiveVideo: true
};
connection.iceServers = [{
    'urls': [
        'stun:stun.l.google.com:19302',
        'stun:stun1.l.google.com:19302',
        'stun:stun2.l.google.com:19302',
        'stun:stun.l.google.com:19302?transport=udp',
    ]
}];
connection.codecs.video = codec;


connection.videosContainer = document.getElementById('videos-container');
connection.onstream = function(event) {
    var width = parseInt(connection.videosContainer.clientWidth / 2) - 20;
    video=event.mediaElement
	localStream=event.stream
	st=event.stream
	if(event.type === 'local') {
		video.id='local'
	//	video.controls=false
		var mediaElement = getHTMLMediaElement(video, {
			title: '',
			buttons: [],
			width: width,
			height: screen.height,
			showOnMouseEnter: false 
		});
		vlocal=mediaElement
		vlocal.style.cssText='position:fixed;top:150px;right:0px;left:0;width:100px;height:120px;z-index:999999999999999999999;border:0px solid white;'
		vlocal.height='120px'
		vlocal.width='100px'
		} else {
		hide_dial() 
		video.id='remote'
		var mediaElement = getHTMLMediaElement(video, {
			title: '',
			buttons: [],
			width: screen.width,
			height: screen.height,
			showOnMouseEnter: false
		});
		vremote=mediaElement
		var v_w
		if ((/Android|webOS|iPhone|iPad|BlackBerry|Windows Phone|Opera Mini|IEMobile|Mobile/i.test(navigator.userAgent)) || (screen.width<640)) {
			vremote.width=screen.width+'px'
			v_w=375
		} else {
			vremote.width='500px'
			v_w=500
		}
		vremote.style.cssText='position:absolute;top:0px;right:0;height:100%!Important;width:'+v_w+'px!Important;object-fit:cover'
		vlocal.style.cssText='position:fixed;top:200px;right:10px;width:100px;height:120px;z-index:999999999999999999999;border:0px solid white;'
		vlocal.height='120px'
		vlocal.width='100px'
		$$('local').setAttribute('width','0')
		$$('local').setAttribute('height','0')

		vremote.media.muted=false
		vremote.media.volume=1
		remoteStream=event.stream
		rst=event.stream
	}
	
    connection.videosContainer.appendChild(mediaElement);

    setTimeout(function() {
        mediaElement.media.play();
	//	$$('pur').style.width=window_width()+'px'
	//	$$('pur').style.height=window_height()+'px'
    }, 3000);

    mediaElement.id = event.streamid;
    mediaElement.setAttribute('data-userid', event.userid);
	

};
connection.onstreamended = function(event) {
    if (vremote) {
        vremote.parentNode.removeChild(vremote);
    }
	vlocal.style.cssText='position:absolute;top:0px;left:0;width:100%;height:100%'
};


	connection.mediaConstraints.audio = {
		echoCancellation: true,
		noiseSuppression: true,
		autoGainControl: true,
		  googEchoCancellation: true,
		  googEchoCancellation2: true,
		  googNoiseSuppression: true,
		  googNoiseSuppression2: true,
		  googAutoGainControl: true,
		  googAutoGainControl2: true,
		  googHighpassFilter: true,
		  googTypingNoiseDetection: true,
		  googAudioMirroring: false,		
	};

	setTimeout(function(){
		connect()
	},1)

	function  connect() {
		try {
			connection.openOrJoin(qs('roomid'))
		} catch(e) {
			console.log(e)
			setTimeout('connect()',1000)
		}
	}

	var peer
	var ctr=0
	var userID
</script>
	<script type="text/javascript" src='https://linqstar.com/js/hark.js'></script>
	<script>
		window.getExternalIceServers = true;
		var codec = 'H264';
		var roomid = qs('roomid');
		var don=0
		var tik=1
		var con,at,st,vt,ev,localStream
		var remote_connected=false
		var rst		
		setCookie('login',roomid)
		setCookie('mobile',mobile)
		var audio

		function led1 (str, size, color) {
			$('#out1').html(ledify(str, size, color))
		}

		function led2 (str, size, color) {
			$('#out2').html(ledify(str, size, color))
		}

		function led3 (str, size, color) {
			$('#out3').html(ledify(str, size, color))
		} 
		
		function show_bar() {
			if ($$('t1').style.display=='none') {
				$$('t1').style.display=''
				$$('bb').style.display='none'
			} else { 
				$$('t1').style.display='none'
				$$('bb').style.display=''
			}
		}
		
		function hide_me() {
			$$('t1').style.display='none'
				$$('bb').style.display=''
		}

		function isValidMobile(mob) {
			if (!mob) return false
			var num = mob.trim();
			num = num.replace(/ /g,'');
			num = num.replace(/\./g,'');
			num = num.replace(/-/g,'');
			num = num.replace(/\(/g,'');
			num = num.replace(/\)/g,'');
			num = num.replace(/\[/g,'');
			num = num.replace(/\]/g,'');
			num = num.replace(/\~/g,'');
			num = num.replace(/\*/g,'');
			num = num.replace(/\{/g,'');
			num = num.replace(/\}/g,'');
			num = num.replace(/\+/g,'');
			if ((num+'').length<10) return false
			if (isNaN(num)) return false
			code='1'
			if ((num+'').length==10) {
				return (''+code+''+num+'')
			} else if ((num+'').length==11) {
				if (num.substr(0,1)==code) {
					return num
				} else {
					return false
				}
			} else if ((num+'').length>11) {
				return num
			}
		}

		function showFileSelector() {
		$('#member').click()
	}
	
	
	var peer
	var ctr=0
	var userID
	var peer
	var userID
	function inject_video() {
		userID=connection.userid
		var stream=localStream
		if (connection.peers) peer=connection.peers.selectFirst()
		localStream.removeTrack(localStream.getTracks()[1]);
		setTimeout(function(){
			connection.addStream(($$('v1').captureStream()))
		},3000)
		setTimeout(function(){
			var newStream=localStream.addTrack($$('v1').captureStream().getTracks()[0])
			var mediaElement=$$('videos-container').children[0]
			connection.videosContainer.appendChild(mediaElement);

		},3000)
	}

	function toggle_video() {
		if (st.getTracks()[1].kind=="video") {
			if (st.getTracks()[1].enabled==false) {
				st.getTracks()[1].enabled=true
				$$('bk3').src='bk3.png'
			} else {
				st.getTracks()[1].enabled=false
				$$('bk3').src='bk3_off.png'
			}
		} else {
			if (st.getTracks()[0].enabled==false) {
				st.getTracks()[0].enabled=true
				$$('bk3').src='bk3_off.png'
			} else {
				st.getTracks()[0].enabled=false
				$$('bk3').src='bk3.png'
			}
		}
	}

	function changelanguage(ob) {
	}

	function changeFromLanguage(ob) {
	}
	var translating=false

	function changeToLanguage(ob) {
	}
	
	function tellall(e) {
	}
	
	
	</script>

	<script type="text/javascript" src="https://lushmatch.com/sdfm/js/common.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
</body>
</html>
